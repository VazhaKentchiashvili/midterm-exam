package com.vazhasapp.midtermexam;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.vazhasapp.midtermexam.databinding.ActivityMainBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentFirstInstructionBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentFourthInstructionBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentHomeBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentInstructionBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentLoginBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentMainBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentNewsDetailsBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentRegistrationBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentSavedNewsBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentSearchNewsBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentSecondInstructionBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentSplashScreenBindingImpl;
import com.vazhasapp.midtermexam.databinding.FragmentThirdInstructionBindingImpl;
import com.vazhasapp.midtermexam.databinding.NewsItemCardViewBindingImpl;
import com.vazhasapp.midtermexam.databinding.NewsVerticalCardViewBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_FRAGMENTFIRSTINSTRUCTION = 2;

  private static final int LAYOUT_FRAGMENTFOURTHINSTRUCTION = 3;

  private static final int LAYOUT_FRAGMENTHOME = 4;

  private static final int LAYOUT_FRAGMENTINSTRUCTION = 5;

  private static final int LAYOUT_FRAGMENTLOGIN = 6;

  private static final int LAYOUT_FRAGMENTMAIN = 7;

  private static final int LAYOUT_FRAGMENTNEWSDETAILS = 8;

  private static final int LAYOUT_FRAGMENTREGISTRATION = 9;

  private static final int LAYOUT_FRAGMENTSAVEDNEWS = 10;

  private static final int LAYOUT_FRAGMENTSEARCHNEWS = 11;

  private static final int LAYOUT_FRAGMENTSECONDINSTRUCTION = 12;

  private static final int LAYOUT_FRAGMENTSPLASHSCREEN = 13;

  private static final int LAYOUT_FRAGMENTTHIRDINSTRUCTION = 14;

  private static final int LAYOUT_NEWSITEMCARDVIEW = 15;

  private static final int LAYOUT_NEWSVERTICALCARDVIEW = 16;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(16);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_first_instruction, LAYOUT_FRAGMENTFIRSTINSTRUCTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_fourth_instruction, LAYOUT_FRAGMENTFOURTHINSTRUCTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_home, LAYOUT_FRAGMENTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_instruction, LAYOUT_FRAGMENTINSTRUCTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_login, LAYOUT_FRAGMENTLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_main, LAYOUT_FRAGMENTMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_news_details, LAYOUT_FRAGMENTNEWSDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_registration, LAYOUT_FRAGMENTREGISTRATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_saved_news, LAYOUT_FRAGMENTSAVEDNEWS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_search_news, LAYOUT_FRAGMENTSEARCHNEWS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_second_instruction, LAYOUT_FRAGMENTSECONDINSTRUCTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_splash_screen, LAYOUT_FRAGMENTSPLASHSCREEN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.fragment_third_instruction, LAYOUT_FRAGMENTTHIRDINSTRUCTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.news_item_card_view, LAYOUT_NEWSITEMCARDVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.vazhasapp.midtermexam.R.layout.news_vertical_card_view, LAYOUT_NEWSVERTICALCARDVIEW);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTFIRSTINSTRUCTION: {
          if ("layout/fragment_first_instruction_0".equals(tag)) {
            return new FragmentFirstInstructionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_first_instruction is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTFOURTHINSTRUCTION: {
          if ("layout/fragment_fourth_instruction_0".equals(tag)) {
            return new FragmentFourthInstructionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_fourth_instruction is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTHOME: {
          if ("layout/fragment_home_0".equals(tag)) {
            return new FragmentHomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTINSTRUCTION: {
          if ("layout/fragment_instruction_0".equals(tag)) {
            return new FragmentInstructionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_instruction is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTLOGIN: {
          if ("layout/fragment_login_0".equals(tag)) {
            return new FragmentLoginBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_login is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTMAIN: {
          if ("layout/fragment_main_0".equals(tag)) {
            return new FragmentMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_main is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTNEWSDETAILS: {
          if ("layout/fragment_news_details_0".equals(tag)) {
            return new FragmentNewsDetailsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_news_details is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTREGISTRATION: {
          if ("layout/fragment_registration_0".equals(tag)) {
            return new FragmentRegistrationBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_registration is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSAVEDNEWS: {
          if ("layout/fragment_saved_news_0".equals(tag)) {
            return new FragmentSavedNewsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_saved_news is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSEARCHNEWS: {
          if ("layout/fragment_search_news_0".equals(tag)) {
            return new FragmentSearchNewsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_search_news is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSECONDINSTRUCTION: {
          if ("layout/fragment_second_instruction_0".equals(tag)) {
            return new FragmentSecondInstructionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_second_instruction is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSPLASHSCREEN: {
          if ("layout/fragment_splash_screen_0".equals(tag)) {
            return new FragmentSplashScreenBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_splash_screen is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTTHIRDINSTRUCTION: {
          if ("layout/fragment_third_instruction_0".equals(tag)) {
            return new FragmentThirdInstructionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_third_instruction is invalid. Received: " + tag);
        }
        case  LAYOUT_NEWSITEMCARDVIEW: {
          if ("layout/news_item_card_view_0".equals(tag)) {
            return new NewsItemCardViewBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for news_item_card_view is invalid. Received: " + tag);
        }
        case  LAYOUT_NEWSVERTICALCARDVIEW: {
          if ("layout/news_vertical_card_view_0".equals(tag)) {
            return new NewsVerticalCardViewBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for news_vertical_card_view is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(4);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "bbcArticle");
      sKeys.put(2, "fabClickListener");
      sKeys.put(3, "newsArticle");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(16);

    static {
      sKeys.put("layout/activity_main_0", com.vazhasapp.midtermexam.R.layout.activity_main);
      sKeys.put("layout/fragment_first_instruction_0", com.vazhasapp.midtermexam.R.layout.fragment_first_instruction);
      sKeys.put("layout/fragment_fourth_instruction_0", com.vazhasapp.midtermexam.R.layout.fragment_fourth_instruction);
      sKeys.put("layout/fragment_home_0", com.vazhasapp.midtermexam.R.layout.fragment_home);
      sKeys.put("layout/fragment_instruction_0", com.vazhasapp.midtermexam.R.layout.fragment_instruction);
      sKeys.put("layout/fragment_login_0", com.vazhasapp.midtermexam.R.layout.fragment_login);
      sKeys.put("layout/fragment_main_0", com.vazhasapp.midtermexam.R.layout.fragment_main);
      sKeys.put("layout/fragment_news_details_0", com.vazhasapp.midtermexam.R.layout.fragment_news_details);
      sKeys.put("layout/fragment_registration_0", com.vazhasapp.midtermexam.R.layout.fragment_registration);
      sKeys.put("layout/fragment_saved_news_0", com.vazhasapp.midtermexam.R.layout.fragment_saved_news);
      sKeys.put("layout/fragment_search_news_0", com.vazhasapp.midtermexam.R.layout.fragment_search_news);
      sKeys.put("layout/fragment_second_instruction_0", com.vazhasapp.midtermexam.R.layout.fragment_second_instruction);
      sKeys.put("layout/fragment_splash_screen_0", com.vazhasapp.midtermexam.R.layout.fragment_splash_screen);
      sKeys.put("layout/fragment_third_instruction_0", com.vazhasapp.midtermexam.R.layout.fragment_third_instruction);
      sKeys.put("layout/news_item_card_view_0", com.vazhasapp.midtermexam.R.layout.news_item_card_view);
      sKeys.put("layout/news_vertical_card_view_0", com.vazhasapp.midtermexam.R.layout.news_vertical_card_view);
    }
  }
}
