package com.vazhasapp.midtermexam.databinding;
import com.vazhasapp.midtermexam.R;
import com.vazhasapp.midtermexam.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class NewsItemCardViewBindingImpl extends NewsItemCardViewBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final android.widget.TextView mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public NewsItemCardViewBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private NewsItemCardViewBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            );
        this.imNewsArticle.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.tvNewsTitle.setTag(null);
        this.tvPublishedDate.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.newsArticle == variableId) {
            setNewsArticle((com.vazhasapp.midtermexam.models.Article) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setNewsArticle(@Nullable com.vazhasapp.midtermexam.models.Article NewsArticle) {
        this.mNewsArticle = NewsArticle;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.newsArticle);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String newsArticlePublishedAt = null;
        java.lang.String newsArticleAuthor = null;
        java.lang.String newsArticleUrlToImage = null;
        java.lang.String newsArticleAuthorToString = null;
        com.vazhasapp.midtermexam.models.Article newsArticle = mNewsArticle;
        java.lang.String newsArticleTitle = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (newsArticle != null) {
                    // read newsArticle.publishedAt
                    newsArticlePublishedAt = newsArticle.getPublishedAt();
                    // read newsArticle.author
                    newsArticleAuthor = newsArticle.getAuthor();
                    // read newsArticle.urlToImage
                    newsArticleUrlToImage = newsArticle.getUrlToImage();
                    // read newsArticle.title
                    newsArticleTitle = newsArticle.getTitle();
                }


                if (newsArticleAuthor != null) {
                    // read newsArticle.author.toString()
                    newsArticleAuthorToString = newsArticleAuthor.toString();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.vazhasapp.midtermexam.adapters.BindingAdaptersKt.setImageFromGlide(this.imNewsArticle, newsArticleUrlToImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, newsArticleAuthorToString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvNewsTitle, newsArticleTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPublishedDate, newsArticlePublishedAt);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): newsArticle
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}