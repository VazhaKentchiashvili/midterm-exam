package com.vazhasapp.midtermexam.databinding;
import com.vazhasapp.midtermexam.R;
import com.vazhasapp.midtermexam.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class NewsVerticalCardViewBindingImpl extends NewsVerticalCardViewBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public NewsVerticalCardViewBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private NewsVerticalCardViewBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.ImSliderNews.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.tvSliderNewsTitle.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.bbcArticle == variableId) {
            setBbcArticle((com.vazhasapp.midtermexam.models.Article) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setBbcArticle(@Nullable com.vazhasapp.midtermexam.models.Article BbcArticle) {
        this.mBbcArticle = BbcArticle;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.bbcArticle);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String bbcArticleTitle = null;
        com.vazhasapp.midtermexam.models.Article bbcArticle = mBbcArticle;
        java.lang.String bbcArticleUrlToImage = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (bbcArticle != null) {
                    // read bbcArticle.title
                    bbcArticleTitle = bbcArticle.getTitle();
                    // read bbcArticle.urlToImage
                    bbcArticleUrlToImage = bbcArticle.getUrlToImage();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.vazhasapp.midtermexam.adapters.BindingAdaptersKt.setImageFromGlide(this.ImSliderNews, bbcArticleUrlToImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvSliderNewsTitle, bbcArticleTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): bbcArticle
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}