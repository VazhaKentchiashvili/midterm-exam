package com.vazhasapp.midtermexam.ui.fragments;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.vazhasapp.midtermexam.R;

public class RegistrationFragmentDirections {
  private RegistrationFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionRegistrationFragmentToLoginFragment() {
    return new ActionOnlyNavDirections(R.id.action_registrationFragment_to_loginFragment);
  }

  @NonNull
  public static NavDirections actionRegistrationFragmentToInstructionFragment() {
    return new ActionOnlyNavDirections(R.id.action_registrationFragment_to_instructionFragment);
  }
}
