package com.vazhasapp.midtermexam.ui.fragments.InstructionFragments;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.vazhasapp.midtermexam.R;
import com.vazhasapp.midtermexam.models.Article;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class SplashScreenFragmentDirections {
  private SplashScreenFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionSplashScreenFragmentToInstructionFragment() {
    return new ActionOnlyNavDirections(R.id.action_splashScreenFragment_to_instructionFragment);
  }

  @NonNull
  public static NavDirections actionSplashFragmentToFirstInstructionFragment() {
    return new ActionOnlyNavDirections(R.id.action_splashFragment_to_firstInstructionFragment);
  }

  @NonNull
  public static ActionSplashFragmentToMainFragment actionSplashFragmentToMainFragment(
      @NonNull Article article) {
    return new ActionSplashFragmentToMainFragment(article);
  }

  @NonNull
  public static NavDirections actionSplashFragmentToRegistrationFragment() {
    return new ActionOnlyNavDirections(R.id.action_splashFragment_to_registrationFragment);
  }

  @NonNull
  public static NavDirections actionSplashFragmentToLoginFragment() {
    return new ActionOnlyNavDirections(R.id.action_splashFragment_to_loginFragment);
  }

  public static class ActionSplashFragmentToMainFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    @SuppressWarnings("unchecked")
    private ActionSplashFragmentToMainFragment(@NonNull Article article) {
      if (article == null) {
        throw new IllegalArgumentException("Argument \"article\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("article", article);
    }

    @NonNull
    @SuppressWarnings("unchecked")
    public ActionSplashFragmentToMainFragment setArticle(@NonNull Article article) {
      if (article == null) {
        throw new IllegalArgumentException("Argument \"article\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("article", article);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("article")) {
        Article article = (Article) arguments.get("article");
        if (Parcelable.class.isAssignableFrom(Article.class) || article == null) {
          __result.putParcelable("article", Parcelable.class.cast(article));
        } else if (Serializable.class.isAssignableFrom(Article.class)) {
          __result.putSerializable("article", Serializable.class.cast(article));
        } else {
          throw new UnsupportedOperationException(Article.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_splashFragment_to_mainFragment;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public Article getArticle() {
      return (Article) arguments.get("article");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionSplashFragmentToMainFragment that = (ActionSplashFragmentToMainFragment) object;
      if (arguments.containsKey("article") != that.arguments.containsKey("article")) {
        return false;
      }
      if (getArticle() != null ? !getArticle().equals(that.getArticle()) : that.getArticle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getArticle() != null ? getArticle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionSplashFragmentToMainFragment(actionId=" + getActionId() + "){"
          + "article=" + getArticle()
          + "}";
    }
  }
}
