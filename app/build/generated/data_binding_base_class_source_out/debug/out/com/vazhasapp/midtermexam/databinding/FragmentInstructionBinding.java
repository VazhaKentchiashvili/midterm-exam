// Generated by data binding compiler. Do not edit!
package com.vazhasapp.midtermexam.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.vazhasapp.midtermexam.R;
import java.lang.Deprecated;
import java.lang.Object;
import me.relex.circleindicator.CircleIndicator3;

public abstract class FragmentInstructionBinding extends ViewDataBinding {
  @NonNull
  public final CircleIndicator3 circleIndicator;

  @NonNull
  public final ViewPager2 myViewPager2;

  protected FragmentInstructionBinding(Object _bindingComponent, View _root, int _localFieldCount,
      CircleIndicator3 circleIndicator, ViewPager2 myViewPager2) {
    super(_bindingComponent, _root, _localFieldCount);
    this.circleIndicator = circleIndicator;
    this.myViewPager2 = myViewPager2;
  }

  @NonNull
  public static FragmentInstructionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_instruction, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentInstructionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentInstructionBinding>inflateInternal(inflater, R.layout.fragment_instruction, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentInstructionBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_instruction, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentInstructionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentInstructionBinding>inflateInternal(inflater, R.layout.fragment_instruction, null, false, component);
  }

  public static FragmentInstructionBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentInstructionBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentInstructionBinding)bind(component, view, R.layout.fragment_instruction);
  }
}
