// Generated by data binding compiler. Do not edit!
package com.vazhasapp.midtermexam.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.vazhasapp.midtermexam.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentFourthInstructionBinding extends ViewDataBinding {
  @NonNull
  public final Button btnInstructionFinish;

  protected FragmentFourthInstructionBinding(Object _bindingComponent, View _root,
      int _localFieldCount, Button btnInstructionFinish) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnInstructionFinish = btnInstructionFinish;
  }

  @NonNull
  public static FragmentFourthInstructionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_fourth_instruction, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentFourthInstructionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentFourthInstructionBinding>inflateInternal(inflater, R.layout.fragment_fourth_instruction, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentFourthInstructionBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_fourth_instruction, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentFourthInstructionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentFourthInstructionBinding>inflateInternal(inflater, R.layout.fragment_fourth_instruction, null, false, component);
  }

  public static FragmentFourthInstructionBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentFourthInstructionBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (FragmentFourthInstructionBinding)bind(component, view, R.layout.fragment_fourth_instruction);
  }
}
