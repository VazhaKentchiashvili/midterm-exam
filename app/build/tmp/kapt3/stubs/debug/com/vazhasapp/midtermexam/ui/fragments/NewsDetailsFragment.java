package com.vazhasapp.midtermexam.ui.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0002J$\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\b\u0010\u001e\u001a\u00020\u0015H\u0016J\u001a\u0010\u001f\u001a\u00020\u00152\u0006\u0010 \u001a\u00020\u00172\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\b\u0010!\u001a\u00020\u0015H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u000b\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006\""}, d2 = {"Lcom/vazhasapp/midtermexam/ui/fragments/NewsDetailsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/vazhasapp/midtermexam/databinding/FragmentNewsDetailsBinding;", "articleArgs", "Lcom/vazhasapp/midtermexam/ui/fragments/MainFragmentArgs;", "getArticleArgs", "()Lcom/vazhasapp/midtermexam/ui/fragments/MainFragmentArgs;", "articleArgs$delegate", "Landroidx/navigation/NavArgsLazy;", "binding", "getBinding", "()Lcom/vazhasapp/midtermexam/databinding/FragmentNewsDetailsBinding;", "viewModels", "Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;", "getViewModels", "()Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;", "viewModels$delegate", "Lkotlin/Lazy;", "init", "", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onViewCreated", "view", "setupWebView", "app_debug"})
public final class NewsDetailsFragment extends androidx.fragment.app.Fragment {
    private com.vazhasapp.midtermexam.databinding.FragmentNewsDetailsBinding _binding;
    private final androidx.navigation.NavArgsLazy articleArgs$delegate = null;
    private final kotlin.Lazy viewModels$delegate = null;
    
    private final com.vazhasapp.midtermexam.databinding.FragmentNewsDetailsBinding getBinding() {
        return null;
    }
    
    private final com.vazhasapp.midtermexam.ui.fragments.MainFragmentArgs getArticleArgs() {
        return null;
    }
    
    private final com.vazhasapp.midtermexam.viewModel.NewsViewModel getViewModels() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void init() {
    }
    
    private final void setupWebView() {
    }
    
    @java.lang.Override()
    public void onDestroyView() {
    }
    
    public NewsDetailsFragment() {
        super();
    }
}