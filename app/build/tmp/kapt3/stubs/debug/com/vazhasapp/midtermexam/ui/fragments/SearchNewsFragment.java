package com.vazhasapp.midtermexam.ui.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0012\u001a\u00020\u0013H\u0002J\b\u0010\u0014\u001a\u00020\u0013H\u0002J$\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u0013H\u0016J\u001a\u0010\u001e\u001a\u00020\u00132\u0006\u0010\u001f\u001a\u00020\u00162\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010 \u001a\u00020\u0013H\u0002J\b\u0010!\u001a\u00020\u0013H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\f\u001a\u00020\r8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\""}, d2 = {"Lcom/vazhasapp/midtermexam/ui/fragments/SearchNewsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/vazhasapp/midtermexam/databinding/FragmentSearchNewsBinding;", "binding", "getBinding", "()Lcom/vazhasapp/midtermexam/databinding/FragmentSearchNewsBinding;", "mainDispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "myAdapter", "Lcom/vazhasapp/midtermexam/adapters/SearchRecyclerAdapter;", "viewModels", "Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;", "getViewModels", "()Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;", "viewModels$delegate", "Lkotlin/Lazy;", "init", "", "listeners", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onViewCreated", "view", "searchingLogic", "setupRecyclerView", "app_debug"})
public final class SearchNewsFragment extends androidx.fragment.app.Fragment {
    private final kotlin.Lazy viewModels$delegate = null;
    private final com.vazhasapp.midtermexam.adapters.SearchRecyclerAdapter myAdapter = null;
    private final kotlinx.coroutines.CoroutineDispatcher mainDispatcher = null;
    private com.vazhasapp.midtermexam.databinding.FragmentSearchNewsBinding _binding;
    
    private final com.vazhasapp.midtermexam.viewModel.NewsViewModel getViewModels() {
        return null;
    }
    
    private final com.vazhasapp.midtermexam.databinding.FragmentSearchNewsBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void init() {
    }
    
    private final void searchingLogic() {
    }
    
    private final void listeners() {
    }
    
    private final void setupRecyclerView() {
    }
    
    @java.lang.Override()
    public void onDestroyView() {
    }
    
    public SearchNewsFragment() {
        super();
    }
}