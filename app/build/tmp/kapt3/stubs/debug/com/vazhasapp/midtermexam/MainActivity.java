package com.vazhasapp.midtermexam;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\fH\u0002J\b\u0010\r\u001a\u00020\fH\u0002J\u0012\u0010\u000e\u001a\u00020\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u0011"}, d2 = {"Lcom/vazhasapp/midtermexam/MainActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "binding", "Lcom/vazhasapp/midtermexam/databinding/ActivityMainBinding;", "newsViewModel", "Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;", "getNewsViewModel", "()Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;", "setNewsViewModel", "(Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;)V", "clearSharedPreferences", "", "init", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class MainActivity extends androidx.appcompat.app.AppCompatActivity {
    private com.vazhasapp.midtermexam.databinding.ActivityMainBinding binding;
    public com.vazhasapp.midtermexam.viewModel.NewsViewModel newsViewModel;
    
    @org.jetbrains.annotations.NotNull()
    public final com.vazhasapp.midtermexam.viewModel.NewsViewModel getNewsViewModel() {
        return null;
    }
    
    public final void setNewsViewModel(@org.jetbrains.annotations.NotNull()
    com.vazhasapp.midtermexam.viewModel.NewsViewModel p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void init() {
    }
    
    private final void clearSharedPreferences() {
    }
    
    public MainActivity() {
        super();
    }
}