package com.vazhasapp.midtermexam.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001b\u0010\u0003\u001a\u00020\u00048FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R#\u0010\t\u001a\n \u000b*\u0004\u0018\u00010\n0\n8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000e\u0010\b\u001a\u0004\b\f\u0010\r\u00a8\u0006\u000f"}, d2 = {"Lcom/vazhasapp/midtermexam/api/NewsApiInstance;", "", "()V", "newsApiService", "Lcom/vazhasapp/midtermexam/api/NewsApi;", "getNewsApiService", "()Lcom/vazhasapp/midtermexam/api/NewsApi;", "newsApiService$delegate", "Lkotlin/Lazy;", "retrofitBuilder", "Lretrofit2/Retrofit;", "kotlin.jvm.PlatformType", "getRetrofitBuilder", "()Lretrofit2/Retrofit;", "retrofitBuilder$delegate", "app_debug"})
public final class NewsApiInstance {
    private static final kotlin.Lazy retrofitBuilder$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.Lazy newsApiService$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.vazhasapp.midtermexam.api.NewsApiInstance INSTANCE = null;
    
    private final retrofit2.Retrofit getRetrofitBuilder() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.vazhasapp.midtermexam.api.NewsApi getNewsApiService() {
        return null;
    }
    
    private NewsApiInstance() {
        super();
    }
}