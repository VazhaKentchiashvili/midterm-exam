package com.vazhasapp.midtermexam.viewModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\bJ\u0012\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u001e0\u0006J\b\u0010\u001f\u001a\u00020\u001bH\u0002J\u000e\u0010 \u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\"J\u0010\u0010#\u001a\u00020\u001b2\u0006\u0010$\u001a\u00020\"H\u0002J\u001c\u0010%\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020\f0\'H\u0002J\u001c\u0010(\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020\f0\'H\u0002J\u0006\u0010)\u001a\u00020*J\u000e\u0010+\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\bR\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00068F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0011R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00068F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0011R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00068F\u00a2\u0006\u0006\u001a\u0004\b\u0019\u0010\u0011\u00a8\u0006,"}, d2 = {"Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;", "Landroidx/lifecycle/ViewModel;", "newsRepository", "Lcom/vazhasapp/midtermexam/repository/NewsRepository;", "(Lcom/vazhasapp/midtermexam/repository/NewsRepository;)V", "_articlesData", "Landroidx/lifecycle/LiveData;", "Landroidx/paging/PagingData;", "Lcom/vazhasapp/midtermexam/models/Article;", "_bbcNewsLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/vazhasapp/midtermexam/utilities/ResponseHandler;", "Lcom/vazhasapp/midtermexam/models/News;", "_newsLiveData", "_searchNews", "articlesData", "getArticlesData", "()Landroidx/lifecycle/LiveData;", "bbcNewsLiveData", "getBbcNewsLiveData", "ioDispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "newsLiveData", "getNewsLiveData", "searchNews", "getSearchNews", "deleteNews", "Lkotlinx/coroutines/Job;", "article", "getAllNews", "", "getBbcNews", "getSearchedNews", "searchKeyWord", "", "getTopHeadlines", "countryCode", "handleResponse", "responseHandler", "Lretrofit2/Response;", "handleSearchResponse", "init", "", "saveNews", "app_debug"})
public final class NewsViewModel extends androidx.lifecycle.ViewModel {
    private final kotlinx.coroutines.CoroutineDispatcher ioDispatcher = null;
    private androidx.lifecycle.MutableLiveData<com.vazhasapp.midtermexam.utilities.ResponseHandler<com.vazhasapp.midtermexam.models.News>> _newsLiveData;
    private androidx.lifecycle.MutableLiveData<com.vazhasapp.midtermexam.utilities.ResponseHandler<com.vazhasapp.midtermexam.models.News>> _searchNews;
    private androidx.lifecycle.MutableLiveData<com.vazhasapp.midtermexam.utilities.ResponseHandler<com.vazhasapp.midtermexam.models.News>> _bbcNewsLiveData;
    private androidx.lifecycle.LiveData<androidx.paging.PagingData<com.vazhasapp.midtermexam.models.Article>> _articlesData;
    private final com.vazhasapp.midtermexam.repository.NewsRepository newsRepository = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.vazhasapp.midtermexam.utilities.ResponseHandler<com.vazhasapp.midtermexam.models.News>> getNewsLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.vazhasapp.midtermexam.utilities.ResponseHandler<com.vazhasapp.midtermexam.models.News>> getSearchNews() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.vazhasapp.midtermexam.utilities.ResponseHandler<com.vazhasapp.midtermexam.models.News>> getBbcNewsLiveData() {
        return null;
    }
    
    public final void init() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<androidx.paging.PagingData<com.vazhasapp.midtermexam.models.Article>> getArticlesData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job saveNews(@org.jetbrains.annotations.NotNull()
    com.vazhasapp.midtermexam.models.Article article) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteNews(@org.jetbrains.annotations.NotNull()
    com.vazhasapp.midtermexam.models.Article article) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.vazhasapp.midtermexam.models.Article>> getAllNews() {
        return null;
    }
    
    private final kotlinx.coroutines.Job getTopHeadlines(java.lang.String countryCode) {
        return null;
    }
    
    private final kotlinx.coroutines.Job getBbcNews() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job getSearchedNews(@org.jetbrains.annotations.NotNull()
    java.lang.String searchKeyWord) {
        return null;
    }
    
    private final com.vazhasapp.midtermexam.utilities.ResponseHandler<com.vazhasapp.midtermexam.models.News> handleResponse(retrofit2.Response<com.vazhasapp.midtermexam.models.News> responseHandler) {
        return null;
    }
    
    private final com.vazhasapp.midtermexam.utilities.ResponseHandler<com.vazhasapp.midtermexam.models.News> handleSearchResponse(retrofit2.Response<com.vazhasapp.midtermexam.models.News> responseHandler) {
        return null;
    }
    
    public NewsViewModel(@org.jetbrains.annotations.NotNull()
    com.vazhasapp.midtermexam.repository.NewsRepository newsRepository) {
        super();
    }
}