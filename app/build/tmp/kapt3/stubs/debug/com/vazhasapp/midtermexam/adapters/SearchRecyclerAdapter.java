package com.vazhasapp.midtermexam.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u001d2\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0002\u001d\u001eB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0006\u0010\u0011\u001a\u00020\u0007J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u001c\u0010\u0014\u001a\u00020\u00072\n\u0010\u0015\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0013H\u0016J\u001c\u0010\u0017\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0013H\u0016J\u001a\u0010\u001b\u001a\u00020\u00072\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005R\u001c\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R(\u0010\b\u001a\u0010\u0012\f\u0012\n \n*\u0004\u0018\u00010\u00060\u00060\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/vazhasapp/midtermexam/adapters/SearchRecyclerAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/vazhasapp/midtermexam/adapters/SearchRecyclerAdapter$SearchRecyclerViewHolder;", "()V", "clickListener", "Lkotlin/Function1;", "Lcom/vazhasapp/midtermexam/models/Article;", "", "differ", "Landroidx/recyclerview/widget/AsyncListDiffer;", "kotlin.jvm.PlatformType", "getDiffer", "()Landroidx/recyclerview/widget/AsyncListDiffer;", "setDiffer", "(Landroidx/recyclerview/widget/AsyncListDiffer;)V", "newsList", "", "clearData", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setArticleClickListener", "listener", "Companion", "SearchRecyclerViewHolder", "app_debug"})
public final class SearchRecyclerAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.vazhasapp.midtermexam.adapters.SearchRecyclerAdapter.SearchRecyclerViewHolder> {
    private final java.util.List<com.vazhasapp.midtermexam.models.Article> newsList = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.recyclerview.widget.AsyncListDiffer<com.vazhasapp.midtermexam.models.Article> differ;
    private kotlin.jvm.functions.Function1<? super com.vazhasapp.midtermexam.models.Article, kotlin.Unit> clickListener;
    private static final androidx.recyclerview.widget.DiffUtil.ItemCallback<com.vazhasapp.midtermexam.models.Article> comparator = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.vazhasapp.midtermexam.adapters.SearchRecyclerAdapter.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.vazhasapp.midtermexam.adapters.SearchRecyclerAdapter.SearchRecyclerViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.vazhasapp.midtermexam.adapters.SearchRecyclerAdapter.SearchRecyclerViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.recyclerview.widget.AsyncListDiffer<com.vazhasapp.midtermexam.models.Article> getDiffer() {
        return null;
    }
    
    public final void setDiffer(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.AsyncListDiffer<com.vazhasapp.midtermexam.models.Article> p0) {
    }
    
    public final void setArticleClickListener(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.vazhasapp.midtermexam.models.Article, kotlin.Unit> listener) {
    }
    
    public final void clearData() {
    }
    
    public SearchRecyclerAdapter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/vazhasapp/midtermexam/adapters/SearchRecyclerAdapter$SearchRecyclerViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/vazhasapp/midtermexam/databinding/NewsItemCardViewBinding;", "(Lcom/vazhasapp/midtermexam/adapters/SearchRecyclerAdapter;Lcom/vazhasapp/midtermexam/databinding/NewsItemCardViewBinding;)V", "bind", "", "item", "Lcom/vazhasapp/midtermexam/models/Article;", "app_debug"})
    public final class SearchRecyclerViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.vazhasapp.midtermexam.databinding.NewsItemCardViewBinding binding = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.vazhasapp.midtermexam.models.Article item) {
        }
        
        public SearchRecyclerViewHolder(@org.jetbrains.annotations.NotNull()
        com.vazhasapp.midtermexam.databinding.NewsItemCardViewBinding binding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/vazhasapp/midtermexam/adapters/SearchRecyclerAdapter$Companion;", "", "()V", "comparator", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lcom/vazhasapp/midtermexam/models/Article;", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}