package com.vazhasapp.midtermexam.ui.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J$\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\b\u0010 \u001a\u00020\u0016H\u0016J\u001a\u0010!\u001a\u00020\u00162\u0006\u0010\"\u001a\u00020\u00192\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\b\u0010#\u001a\u00020\u0016H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0010\u0010\u0011R\u000e\u0010\u0014\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"}, d2 = {"Lcom/vazhasapp/midtermexam/ui/fragments/SavedNewsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/vazhasapp/midtermexam/databinding/FragmentSavedNewsBinding;", "binding", "getBinding", "()Lcom/vazhasapp/midtermexam/databinding/FragmentSavedNewsBinding;", "itemTouchHelper", "Landroidx/recyclerview/widget/ItemTouchHelper$SimpleCallback;", "getItemTouchHelper", "()Landroidx/recyclerview/widget/ItemTouchHelper$SimpleCallback;", "myAdapter", "Lcom/vazhasapp/midtermexam/adapters/NewsRecyclerAdapter;", "newsViewModel", "Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;", "getNewsViewModel", "()Lcom/vazhasapp/midtermexam/viewModel/NewsViewModel;", "newsViewModel$delegate", "Lkotlin/Lazy;", "savedNewsRecyclerAdapter", "init", "", "listeners", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onViewCreated", "view", "setupRecyclerView", "app_debug"})
public final class SavedNewsFragment extends androidx.fragment.app.Fragment {
    private com.vazhasapp.midtermexam.databinding.FragmentSavedNewsBinding _binding;
    private final com.vazhasapp.midtermexam.adapters.NewsRecyclerAdapter savedNewsRecyclerAdapter = null;
    private final com.vazhasapp.midtermexam.adapters.NewsRecyclerAdapter myAdapter = null;
    private final kotlin.Lazy newsViewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.recyclerview.widget.ItemTouchHelper.SimpleCallback itemTouchHelper = null;
    
    private final com.vazhasapp.midtermexam.databinding.FragmentSavedNewsBinding getBinding() {
        return null;
    }
    
    private final com.vazhasapp.midtermexam.viewModel.NewsViewModel getNewsViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void init() {
    }
    
    private final void listeners() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.recyclerview.widget.ItemTouchHelper.SimpleCallback getItemTouchHelper() {
        return null;
    }
    
    private final void setupRecyclerView() {
    }
    
    @java.lang.Override()
    public void onDestroyView() {
    }
    
    public SavedNewsFragment() {
        super();
    }
}