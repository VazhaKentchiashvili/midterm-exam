package com.vazhasapp.midtermexam.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u0013B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0007\u001a\u00020\bH\u0016J\u001e\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0018\u00010\u0002R\u00020\u00002\u0006\u0010\f\u001a\u00020\bH\u0016J\u0016\u0010\r\u001a\u00060\u0002R\u00020\u00002\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016J\u0014\u0010\u0010\u001a\u00020\n2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u0012R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/vazhasapp/midtermexam/adapters/AutoSlideAdapter;", "Lcom/smarteist/autoimageslider/SliderViewAdapter;", "Lcom/vazhasapp/midtermexam/adapters/AutoSlideAdapter$AutoSlideViewHolder;", "()V", "sliderList", "", "Lcom/vazhasapp/midtermexam/models/Article;", "getCount", "", "onBindViewHolder", "", "viewHolder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "setBbcNewsList", "enteredBbcNews", "", "AutoSlideViewHolder", "app_debug"})
public final class AutoSlideAdapter extends com.smarteist.autoimageslider.SliderViewAdapter<com.vazhasapp.midtermexam.adapters.AutoSlideAdapter.AutoSlideViewHolder> {
    private final java.util.List<com.vazhasapp.midtermexam.models.Article> sliderList = null;
    
    @java.lang.Override()
    public int getCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.vazhasapp.midtermexam.adapters.AutoSlideAdapter.AutoSlideViewHolder onCreateViewHolder(@org.jetbrains.annotations.Nullable()
    android.view.ViewGroup parent) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.Nullable()
    com.vazhasapp.midtermexam.adapters.AutoSlideAdapter.AutoSlideViewHolder viewHolder, int position) {
    }
    
    public final void setBbcNewsList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.vazhasapp.midtermexam.models.Article> enteredBbcNews) {
    }
    
    public AutoSlideAdapter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 2}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/vazhasapp/midtermexam/adapters/AutoSlideAdapter$AutoSlideViewHolder;", "Lcom/smarteist/autoimageslider/SliderViewAdapter$ViewHolder;", "binding", "Lcom/vazhasapp/midtermexam/databinding/NewsVerticalCardViewBinding;", "(Lcom/vazhasapp/midtermexam/adapters/AutoSlideAdapter;Lcom/vazhasapp/midtermexam/databinding/NewsVerticalCardViewBinding;)V", "bind", "", "item", "Lcom/vazhasapp/midtermexam/models/Article;", "app_debug"})
    public final class AutoSlideViewHolder extends com.smarteist.autoimageslider.SliderViewAdapter.ViewHolder {
        private final com.vazhasapp.midtermexam.databinding.NewsVerticalCardViewBinding binding = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.vazhasapp.midtermexam.models.Article item) {
        }
        
        public AutoSlideViewHolder(@org.jetbrains.annotations.NotNull()
        com.vazhasapp.midtermexam.databinding.NewsVerticalCardViewBinding binding) {
            super(null);
        }
    }
}