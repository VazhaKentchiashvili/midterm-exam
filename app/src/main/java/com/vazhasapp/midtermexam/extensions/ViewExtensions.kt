package com.vazhasapp.midtermexam.extensions

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

fun SwipeRefreshLayout.stopRefresh() {
    isRefreshing = false
}