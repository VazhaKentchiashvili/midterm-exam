package com.vazhasapp.midtermexam.ui.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.vazhasapp.midtermexam.R
import com.vazhasapp.midtermexam.databinding.FragmentRegistrationBinding

class RegistrationFragment : Fragment() {

    private var _binding: FragmentRegistrationBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_registration, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
         checkEnteredInfo()
    }

    private fun checkEnteredInfo() {

        val auth = FirebaseAuth.getInstance()

        binding.btnRegister.setOnClickListener {
            val userName = binding.etRegisterUsername.text.toString()
            val email = binding.etRegisterEmail.text.toString()
            val password = binding.etRegisterPassword.text.toString()

            if (email.isNullOrBlank() && password.isNullOrBlank()) {
                Toast.makeText(requireContext(), "Fill all required fields", Toast.LENGTH_SHORT)
                    .show()
            } else {
                //saveIfUserIsRegistered()
                auth.createUserWithEmailAndPassword(email, password)
                findNavController().navigate(R.id.action_registrationFragment_to_instructionFragment)
                Toast.makeText(requireContext(), "Account successfully created", Toast.LENGTH_SHORT).show()
            }
        }
    }

//    private fun saveIfUserIsRegistered() {
//        val sharedPreferences =
//            requireActivity().getSharedPreferences("Statuses", Context.MODE_PRIVATE)
//        val editor = sharedPreferences.edit()
//        editor.putBoolean("isRegistered", true)
//        editor.apply()
//    }
}