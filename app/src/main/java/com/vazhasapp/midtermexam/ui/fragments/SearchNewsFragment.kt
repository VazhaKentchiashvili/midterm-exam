package com.vazhasapp.midtermexam.ui.fragments

import android.os.Bundle
import android.util.Log.d
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.vazhasapp.midtermexam.R
import com.vazhasapp.midtermexam.adapters.NewsRecyclerAdapter
import com.vazhasapp.midtermexam.adapters.SearchRecyclerAdapter
import com.vazhasapp.midtermexam.databinding.FragmentSearchNewsBinding
import com.vazhasapp.midtermexam.extensions.stopRefresh
import com.vazhasapp.midtermexam.utilities.ResponseHandler
import com.vazhasapp.midtermexam.viewModel.NewsViewModel
import kotlinx.coroutines.*

class SearchNewsFragment : Fragment() {

    private val viewModels: NewsViewModel by activityViewModels()
    private val myAdapter = SearchRecyclerAdapter()
    private val mainDispatcher: CoroutineDispatcher = Dispatchers.Main

    private var _binding: FragmentSearchNewsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_search_news, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        listeners()
        searchingLogic()
        setupRecyclerView()

        myAdapter.setArticleClickListener {
            val direction = MainFragmentDirections.actionMainFragmentToNewsDetailsFragment2(it)
            findNavController().navigate(direction)
        }
    }

    private fun searchingLogic() {
        binding.etSearch.addTextChangedListener { editable ->
            lifecycleScope.launch(mainDispatcher) {
                editable?.let {
                    if (editable.toString().isNotEmpty()) {
                        viewModels.getSearchedNews(it.toString())
                    } else {
                        myAdapter.clearData()
                    }
                }
            }
        }
    }

    private fun listeners() {
        viewModels.searchNews.observe(viewLifecycleOwner, { response ->
            when (response) {
                is ResponseHandler.Success -> {
                    binding.swipeRefresh.stopRefresh()
                    response.data?.let {
                        myAdapter.differ.submitList(it.articles)
                    }
                }
                is ResponseHandler.Error -> {
                    response.errorMessage?.let {
                        d("Http Error", it)
                    }
                }
                is ResponseHandler.Loading -> {
                    binding.swipeRefresh.stopRefresh()
                }
            }
        })
    }

    private fun setupRecyclerView() {
        binding.rvSearchNews.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}