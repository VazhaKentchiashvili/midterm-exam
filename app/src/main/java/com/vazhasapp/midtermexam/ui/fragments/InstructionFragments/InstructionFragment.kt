package com.vazhasapp.midtermexam.ui.fragments.InstructionFragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.vazhasapp.midtermexam.R
import com.vazhasapp.midtermexam.adapters.CustomViewPagerAdapter
import com.vazhasapp.midtermexam.databinding.FragmentInstructionBinding

class InstructionFragment : Fragment() {

    private var _binding: FragmentInstructionBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_instruction, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        val fragmentList = mutableListOf<Fragment>()
        fragmentList.add(FirstInstructionFragment())
        fragmentList.add(SecondInstructionFragment())
        fragmentList.add(ThirdInstructionFragment())
        fragmentList.add(FourthInstructionFragment())
        binding.myViewPager2.adapter = CustomViewPagerAdapter(
            fragmentList,
            this
        )
        binding.circleIndicator.setViewPager(binding.myViewPager2)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}