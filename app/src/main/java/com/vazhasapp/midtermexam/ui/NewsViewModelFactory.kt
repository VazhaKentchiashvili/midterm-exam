package com.vazhasapp.midtermexam.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vazhasapp.midtermexam.repository.NewsRepository
import com.vazhasapp.midtermexam.viewModel.NewsViewModel

class NewsViewModelFactory(
    private val newsRepository: NewsRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewsViewModel(newsRepository) as T
    }
}