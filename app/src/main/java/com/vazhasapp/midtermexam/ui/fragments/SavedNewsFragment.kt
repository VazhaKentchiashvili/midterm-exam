package com.vazhasapp.midtermexam.ui.fragments

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.vazhasapp.midtermexam.R
import com.vazhasapp.midtermexam.adapters.NewsRecyclerAdapter
import com.vazhasapp.midtermexam.databinding.FragmentSavedNewsBinding
import com.vazhasapp.midtermexam.viewModel.NewsViewModel
import java.time.Duration

class SavedNewsFragment : Fragment() {

    private var _binding: FragmentSavedNewsBinding? = null
    private val binding get() = _binding!!

    private val savedNewsRecyclerAdapter = NewsRecyclerAdapter()
    private val myAdapter = NewsRecyclerAdapter()

    private val newsViewModel: NewsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_saved_news, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        listeners()
        setupRecyclerView()
        ItemTouchHelper(itemTouchHelper).attachToRecyclerView(binding.rvSavedNews)

        myAdapter.setArticleClickListener {
            val direction = MainFragmentDirections.actionMainFragmentToNewsDetailsFragment2(it)
            findNavController().navigate(direction)
        }
    }

    private fun listeners() {
        newsViewModel.getAllNews().observe(viewLifecycleOwner, {
            savedNewsRecyclerAdapter.differ.submitList(it)
        })
    }

    val itemTouchHelper = object : ItemTouchHelper.SimpleCallback(
        ItemTouchHelper.UP or ItemTouchHelper.DOWN,
        ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
    ) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            binding.rvSavedNews.rootView.animate().alpha(0f).duration = 5000
            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val newsPosition = viewHolder.adapterPosition
            val currentNews = savedNewsRecyclerAdapter.differ.currentList[newsPosition]
            newsViewModel.deleteNews(currentNews)
            Snackbar.make(requireView(),
                "News successfully deleted",
                Snackbar.LENGTH_SHORT).apply {
                    setAction("Undo") {
                        newsViewModel.saveNews(currentNews)
                    }
                show()
            }
        }
    }

    private fun setupRecyclerView() {
        binding.rvSavedNews.apply {
            adapter = savedNewsRecyclerAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}