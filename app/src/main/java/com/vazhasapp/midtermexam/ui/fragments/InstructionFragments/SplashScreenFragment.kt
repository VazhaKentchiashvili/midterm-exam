package com.vazhasapp.midtermexam.ui.fragments.InstructionFragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.vazhasapp.midtermexam.R
import com.vazhasapp.midtermexam.viewModel.NewsViewModel
import kotlinx.coroutines.*

class SplashScreenFragment : Fragment() {

    val viewModel: NewsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        CoroutineScope(Dispatchers.Main).launch {
            if (checkIfInstructionHaveSeen()) {
                delay(3000)
                findNavController().navigate(R.id.action_splashFragment_to_mainFragment)
            } else {
                findNavController().navigate(R.id.action_splashScreenFragment_to_instructionFragment)
            }
        }
    }

    private fun checkIfInstructionHaveSeen(): Boolean {
        val sharedPreferences =
            requireActivity().getSharedPreferences("Statuses", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean("InstructionFinished", false)
    }
}