package com.vazhasapp.midtermexam.ui

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.vazhasapp.midtermexam.api.NewsApiInstance
import com.vazhasapp.midtermexam.models.Article
import com.vazhasapp.midtermexam.models.News
import com.vazhasapp.midtermexam.utilities.Constants.API_KEY

class Paging : PagingSource<Int, Article>() {

    override fun getRefreshKey(state: PagingState<Int, Article>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Article> {
        return try {
            val page = params.key ?: 1
            val response = NewsApiInstance.newsApiService.getTopHeadlines("us", page, API_KEY)
            val articlesData = mutableListOf<Article>()
            val articles = response.body()?.articles ?: emptyList()
            articlesData.addAll(articles)

            LoadResult.Page(
                data = articlesData,
                prevKey = if (page == 1) null else page,
                nextKey = if (articles.isEmpty()) null else page + 1
            )
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }
}
