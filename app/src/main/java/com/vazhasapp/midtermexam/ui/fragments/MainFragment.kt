package com.vazhasapp.midtermexam.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.vazhasapp.midtermexam.R
import com.vazhasapp.midtermexam.adapters.CustomViewPagerAdapter
import com.vazhasapp.midtermexam.databinding.FragmentMainBinding
import com.vazhasapp.midtermexam.db.NewsDatabase
import com.vazhasapp.midtermexam.repository.NewsRepository
import com.vazhasapp.midtermexam.ui.NewsViewModelFactory
import com.vazhasapp.midtermexam.viewModel.NewsViewModel

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private lateinit var newsViewModel: NewsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        setupViewPager()
        mergeBottomNavAndViewPager()

    }

    private fun setupViewPager() {
        val mainFragmentLists = mutableListOf<Fragment>()
        mainFragmentLists.add(HomeFragment())
        mainFragmentLists.add(SavedNewsFragment())
        mainFragmentLists.add(SearchNewsFragment())

        binding.mainViewPager.adapter = CustomViewPagerAdapter(mainFragmentLists, this)
    }

    private fun chooseMenuItem(position: Int) {
        binding.bottomNav.menu.getItem(position).isChecked = true
    }

    private fun mergeBottomNavAndViewPager() {
        binding.bottomNav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menuHome -> binding.mainViewPager.currentItem = 0
                R.id.menuSavedNews -> binding.mainViewPager.currentItem = 1
                R.id.menuSearchNews -> binding.mainViewPager.currentItem = 2
            }
            true
        }

        binding.mainViewPager.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                chooseMenuItem(position)
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}