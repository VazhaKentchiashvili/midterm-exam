package com.vazhasapp.midtermexam.ui.fragments.InstructionFragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.vazhasapp.midtermexam.R
import com.vazhasapp.midtermexam.databinding.FragmentFourthInstructionBinding

class FourthInstructionFragment : Fragment() {

    private var _binding: FragmentFourthInstructionBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_fourth_instruction,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        binding.btnInstructionFinish.setOnClickListener {
            findNavController().navigate(R.id.action_instructionFragment_to_mainFragment)
            saveInstructionSeenStatus()
        }
    }

    private fun saveInstructionSeenStatus() {
        val sharedPreferences =
            requireActivity().getSharedPreferences("Statuses", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean("InstructionFinished", true)
        editor.apply()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}