package com.vazhasapp.midtermexam.ui.fragments

import android.os.Bundle
import android.util.Log.d
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.vazhasapp.midtermexam.MainActivity
import com.vazhasapp.midtermexam.R
import com.vazhasapp.midtermexam.adapters.AutoSlideAdapter
import com.vazhasapp.midtermexam.adapters.NewsRecyclerAdapter
import com.vazhasapp.midtermexam.databinding.FragmentHomeBinding
import com.vazhasapp.midtermexam.extensions.stopRefresh
import com.vazhasapp.midtermexam.utilities.ResponseHandler
import com.vazhasapp.midtermexam.viewModel.NewsViewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val myAdapter = NewsRecyclerAdapter()
    private val slideAdapter = AutoSlideAdapter()

    val newsViewModel: NewsViewModel by activityViewModels() //(activity as MainActivity).newsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {

        setupRefreshListener()
        setupRecyclerView()
        setupAutoSliderView()

        listeners()

        newsViewModel.init()
        myAdapter.setArticleClickListener {
            val direction = MainFragmentDirections.actionMainFragmentToNewsDetailsFragment2(it)
            findNavController().navigate(direction)
        }
    }


    private fun listeners() {
        // Top Headline News
        newsViewModel.newsLiveData.observe(viewLifecycleOwner, { response ->
            when (response) {
                is ResponseHandler.Success -> {
                    binding.swipeRefresh.stopRefresh()
                    response.data?.let { myAdapter.differ.submitList(it.articles) }
                }
                is ResponseHandler.Error -> response.errorMessage?.let { d("Error", it) }
                is ResponseHandler.Loading -> binding.swipeRefresh.stopRefresh()
            }
        })

        // Bbc Top News
        newsViewModel.bbcNewsLiveData.observe(viewLifecycleOwner, { response ->
            when (response) {
                is ResponseHandler.Success -> {
                    binding.swipeRefresh.stopRefresh()
                    response.data?.let { slideAdapter.setBbcNewsList(it.articles) }
                }
                is ResponseHandler.Error -> response.errorMessage?.let { d("Error", it) }
                is ResponseHandler.Loading -> binding.swipeRefresh.isRefreshing = true
            }
        })

        newsViewModel.articlesData.observe(viewLifecycleOwner, {
            myAdapter.submitData(lifecycle, it)
        })

    }

    private fun setupRefreshListener() {
        binding.swipeRefresh.setOnRefreshListener {
            myAdapter.clearData()
            newsViewModel.init()
            myAdapter.notifyDataSetChanged()
        }
    }

    private fun setupRecyclerView() {
        binding.mainRecyclerView.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    private fun setupAutoSliderView() {
        binding.imageSlider.apply {
            setSliderAdapter(slideAdapter)
            setIndicatorAnimation(IndicatorAnimationType.WORM)
            setSliderTransformAnimation(SliderAnimations.DEPTHTRANSFORMATION)
            startAutoCycle()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}