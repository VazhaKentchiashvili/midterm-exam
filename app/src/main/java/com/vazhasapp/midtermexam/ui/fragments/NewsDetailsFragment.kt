package com.vazhasapp.midtermexam.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.vazhasapp.midtermexam.R
import com.vazhasapp.midtermexam.databinding.FragmentNewsDetailsBinding
import com.vazhasapp.midtermexam.viewModel.NewsViewModel

class NewsDetailsFragment : Fragment() {

    private var _binding: FragmentNewsDetailsBinding? = null
    private val binding get() = _binding!!

    private val articleArgs: MainFragmentArgs by navArgs()

    private val viewModels: NewsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_news_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        setupWebView()

        binding.setFabClickListener {
            viewModels.saveNews(articleArgs.article)
            Snackbar.make(it, "News successfully saved", Snackbar.LENGTH_SHORT).show()
        }

        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setupWebView() {
        binding.wvFragmentDetails.apply {
            webViewClient = WebViewClient()
            loadUrl(articleArgs.article.url.toString())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}