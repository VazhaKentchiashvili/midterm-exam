package com.vazhasapp.midtermexam

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.vazhasapp.midtermexam.databinding.ActivityMainBinding
import com.vazhasapp.midtermexam.db.NewsDatabase
import com.vazhasapp.midtermexam.repository.NewsRepository
import com.vazhasapp.midtermexam.ui.NewsViewModelFactory
import com.vazhasapp.midtermexam.viewModel.NewsViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    lateinit var newsViewModel: NewsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.activity_main, null, false)
        setContentView(binding.root)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        //clearSharedPreferences()
        init()
    }

    private fun init() {
        val newsRepository = NewsRepository(NewsDatabase(this))
        val viewModelFactory = NewsViewModelFactory(newsRepository)
        newsViewModel = ViewModelProvider(this, viewModelFactory).get(NewsViewModel::class.java)
    }

    private fun clearSharedPreferences() {
        val sharedPreferences = getSharedPreferences("Statuses", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }
}