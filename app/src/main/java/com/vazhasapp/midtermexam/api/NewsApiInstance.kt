package com.vazhasapp.midtermexam.api

import com.vazhasapp.midtermexam.utilities.Constants.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NewsApiInstance {

    private val retrofitBuilder by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val newsApiService: NewsApi by lazy {
        retrofitBuilder.create(NewsApi::class.java)
    }
}