package com.vazhasapp.midtermexam.api

import com.vazhasapp.midtermexam.models.News
import com.vazhasapp.midtermexam.utilities.Constants.API_KEY
import com.vazhasapp.midtermexam.utilities.Constants.BBC_NEWS_ENDPOINT
import com.vazhasapp.midtermexam.utilities.Constants.EVERYTHING_ENDPOINT
import com.vazhasapp.midtermexam.utilities.Constants.TOP_HEADLINES_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    @GET(TOP_HEADLINES_ENDPOINT)
    suspend fun getTopHeadlines(
        @Query("country")
        countryCode: String = "us",

        @Query("page")
        pageNumber: Int = 1,

        @Query("apiKey")
        apiKey: String = API_KEY

    ): Response<News>

    @GET(BBC_NEWS_ENDPOINT)
    suspend fun bccTopNews(
        @Query("sources")
        searchKeyWord: String = "bbc-news",

        @Query("page")
        pageNumber: Int = 1,

        @Query("apiKey")
        apiKey: String = API_KEY

    ): Response<News>


    @GET(EVERYTHING_ENDPOINT)
    suspend fun searchNews(
        @Query("q")
        searchKeyWord: String,

        @Query("page")
        pageNumber: Int = 1,

        @Query("apiKey")
        apiKey: String = API_KEY

    ): Response<News>
}