package com.vazhasapp.midtermexam.utilities

object Constants {
    const val BASE_URL = "https://newsapi.org/"
    const val TOP_HEADLINES_ENDPOINT = "v2/top-headlines"
    const val EVERYTHING_ENDPOINT = "v2/everything"
    const val BBC_NEWS_ENDPOINT = "v2/top-headlines"
    const val API_KEY = "5d7ed0692c5842b9a20ebc54a3397c23"
}