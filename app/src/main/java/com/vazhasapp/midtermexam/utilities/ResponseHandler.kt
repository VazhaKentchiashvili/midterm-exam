package com.vazhasapp.midtermexam.utilities

sealed class ResponseHandler<T>(
    val data: T? = null,
    val errorMessage: String? = null,
) {
    class Success<T>(data: T?) : ResponseHandler<T>(data)
    class Error<T>(errorMessage: String, data: T? = null) : ResponseHandler<T>(data, errorMessage)
    class Loading<T> : ResponseHandler<T>()
}