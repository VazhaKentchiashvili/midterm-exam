package com.vazhasapp.midtermexam.utilities

import androidx.room.TypeConverter
import com.vazhasapp.midtermexam.models.Source

class TypeConverter {

    @TypeConverter
    fun sourceToString(source: Source): String {
         return source.name!!
    }

    @TypeConverter
    fun stringToSource(name: String): Source {
        return Source(name, name)
    }
}