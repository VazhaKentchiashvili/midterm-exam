package com.vazhasapp.midtermexam.repository

import com.vazhasapp.midtermexam.api.NewsApiInstance
import com.vazhasapp.midtermexam.db.NewsDatabase
import com.vazhasapp.midtermexam.models.Article

class NewsRepository(
    private val db: NewsDatabase
) {
    suspend fun getApiResponse(countryCode: String) =
        NewsApiInstance.newsApiService.getTopHeadlines(countryCode)

    suspend fun getBbcTopNews(source: String) =
        NewsApiInstance.newsApiService.bccTopNews(source)

    suspend fun searchNews(query: String) =
        NewsApiInstance.newsApiService.searchNews(query)

    suspend fun saveNews(news: Article) = db.newsDao().addNews(news)

    suspend fun deleteNews(news: Article) = db.newsDao().delete(news)

    fun getAllNews() = db.newsDao().getAllNews()
}