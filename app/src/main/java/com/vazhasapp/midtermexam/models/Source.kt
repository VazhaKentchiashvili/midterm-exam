package com.vazhasapp.midtermexam.models

data class Source(
    val id: Any?,
    val name: String?
)