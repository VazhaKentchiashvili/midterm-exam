package com.vazhasapp.midtermexam.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.smarteist.autoimageslider.SliderViewAdapter
import com.vazhasapp.midtermexam.databinding.NewsVerticalCardViewBinding
import com.vazhasapp.midtermexam.models.Article

class AutoSlideAdapter() : SliderViewAdapter<AutoSlideAdapter.AutoSlideViewHolder>() {

    private val sliderList = mutableListOf<Article>()

    override fun getCount() = sliderList.size

    override fun onCreateViewHolder(parent: ViewGroup?): AutoSlideViewHolder {
        return AutoSlideViewHolder(
            NewsVerticalCardViewBinding.inflate(
                LayoutInflater.from(parent?.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: AutoSlideViewHolder?, position: Int) {
        viewHolder?.bind(sliderList[position])
    }

    inner class AutoSlideViewHolder(private val binding: NewsVerticalCardViewBinding) :
        SliderViewAdapter.ViewHolder(binding.root) {

            fun bind(item: Article) {
                binding.bbcArticle = item
                binding.executePendingBindings()
            }
    }

    fun setBbcNewsList(enteredBbcNews: List<Article>) {
        sliderList.clear()
        sliderList.addAll(enteredBbcNews)
        notifyDataSetChanged()
    }
}