package com.vazhasapp.midtermexam.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.midtermexam.databinding.NewsItemCardViewBinding
import com.vazhasapp.midtermexam.models.Article

class SearchRecyclerAdapter : RecyclerView.Adapter<SearchRecyclerAdapter.SearchRecyclerViewHolder>() {

    private val newsList = mutableListOf<Article>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SearchRecyclerViewHolder(
            NewsItemCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: SearchRecyclerViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount() = differ.currentList.size

    inner class SearchRecyclerViewHolder(private val binding: NewsItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Article) {
            binding.newsArticle = item
            binding.executePendingBindings()
            itemView.setOnClickListener { clickListener?.let { it(item) } }
        }
    }

    var differ = AsyncListDiffer(this, comparator)

    companion object {
        private val comparator = object : DiffUtil.ItemCallback<Article>() {
            override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
                return oldItem.url == newItem.url
            }

            override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
                return oldItem == newItem
            }
        }
    }

    private var clickListener: ((Article) -> Unit)? = null

    fun setArticleClickListener(listener: (Article) -> Unit) {
        clickListener = listener
    }

    fun clearData() {
        newsList.clear()
        notifyDataSetChanged()
    }

}