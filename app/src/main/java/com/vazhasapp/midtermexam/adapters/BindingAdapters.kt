package com.vazhasapp.midtermexam.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.vazhasapp.midtermexam.R

@BindingAdapter("setImage")
fun setImageView(view: ImageView, imageId: Int) {
    view.setImageResource(imageId)
}

@BindingAdapter("imageFromGlide")
fun setImageFromGlide(view: ImageView, imageUrl: String?) {
    if (imageUrl != null) {
        Glide.with(view.context).load(imageUrl).into(view)
    } else {
        return
    }
}