package com.vazhasapp.midtermexam.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.vazhasapp.midtermexam.models.Article

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addNews(news: Article)

    @Query("SELECT * FROM news_table")
    fun getAllNews(): LiveData<List<Article>>

    @Delete
    suspend fun delete(news: Article)
}