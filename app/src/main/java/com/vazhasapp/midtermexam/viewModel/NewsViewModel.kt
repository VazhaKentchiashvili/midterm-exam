package com.vazhasapp.midtermexam.viewModel

import androidx.lifecycle.*
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.google.android.material.snackbar.Snackbar
import com.vazhasapp.midtermexam.models.Article
import com.vazhasapp.midtermexam.models.News
import com.vazhasapp.midtermexam.repository.NewsRepository
import com.vazhasapp.midtermexam.ui.Paging
import com.vazhasapp.midtermexam.utilities.ResponseHandler
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class NewsViewModel(
    private val newsRepository: NewsRepository
) : ViewModel() {



    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    // Daily TopHeadlines News
    private var _newsLiveData = MutableLiveData<ResponseHandler<News>>()
    val newsLiveData: LiveData<ResponseHandler<News>> get() = _newsLiveData

    // Search News
    private var _searchNews = MutableLiveData<ResponseHandler<News>>()
    val searchNews: LiveData<ResponseHandler<News>> get() = _searchNews

    // BBC Top News
    private var _bbcNewsLiveData = MutableLiveData<ResponseHandler<News>>()
    val bbcNewsLiveData: LiveData<ResponseHandler<News>> get() = _bbcNewsLiveData

    fun init() {
        getTopHeadlines("us")
        getBbcNews()
    }

    private var _articlesData = Pager(PagingConfig(pageSize = 5)) {
        Paging()
    }.liveData
    val articlesData get() = _articlesData

    fun saveNews(article: Article) = viewModelScope.launch(ioDispatcher) {
        newsRepository.saveNews(article)
    }

    fun deleteNews(article: Article) = viewModelScope.launch(ioDispatcher) {
        newsRepository.deleteNews(article)
    }

    fun getAllNews() = newsRepository.getAllNews()

    private fun getTopHeadlines(countryCode: String) = viewModelScope.launch(ioDispatcher) {
        _newsLiveData.postValue(ResponseHandler.Loading())

        val response = newsRepository.getApiResponse(countryCode)
        _newsLiveData.postValue(handleResponse(response))
    }

    private fun getBbcNews() = viewModelScope.launch(ioDispatcher) {
        _bbcNewsLiveData.postValue(ResponseHandler.Loading())

        val response = newsRepository.getBbcTopNews("bbc-news")
        _bbcNewsLiveData.postValue(handleResponse(response))
    }

    fun getSearchedNews(searchKeyWord: String) = viewModelScope.launch(ioDispatcher) {
        _searchNews.postValue(ResponseHandler.Loading())

        val response = newsRepository.searchNews(searchKeyWord)
        _searchNews.postValue(handleSearchResponse(response))
    }

    private fun handleResponse(responseHandler: Response<News>): ResponseHandler<News> {
        if (responseHandler.isSuccessful) {
            responseHandler.body().let { result ->
                return ResponseHandler.Success(result)
            }
        }
        return ResponseHandler.Error(responseHandler.message())
    }

    private fun handleSearchResponse(responseHandler: Response<News>): ResponseHandler<News> {
        if (responseHandler.isSuccessful) {
            responseHandler.body().let { result ->
                return ResponseHandler.Success(result)
            }
        }
        return ResponseHandler.Error(responseHandler.message())
    }
}